@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Users</div>

                    <div class="card-body">

                        <ul>
                            @foreach($users as $user)

                                <li>
                                    
                                    {{ $user->name }}

                                    <form action="{{ route('chat.start') }}" method="post" class="d-inline">

                                        @csrf

                                        <input type="hidden" name="partner" value="{{ $user->id }}">

                                        <button type="submit" class="btn btn-primary btn-sm">chat</button>

                                    </form>
                                
                                </li>

                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
