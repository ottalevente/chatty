<?php

namespace App\Models\Chat\Traits;

use App\Models\Chat\Message;
use App\Models\Users\User;

/**
 * Trait RoomTrait
 *
 * @package App\Models\Chat\Traits
 */
trait RoomTrait
{

    /**
     * @return mixed
     */
    public function messages()
    {
        return $this->hasMany(Message::class, 'room_id', 'id')
            ->orderby('created_at');
    }

    /**
     * @return mixed
     */
    public function participants()
    {
        return $this->belongsToMany(User::class, 'chat_room_user', 'room_id', 'user_id')
            ->where('user_id', '!=', \Auth::id())
            ->withPivot('seen');
    }
    
}