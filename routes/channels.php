<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('chat.{room}', function($user, \App\Models\Chat\Room $room) {
    if ($room->participants->contains($user)) {
        return ['name' => $user->name,];
    }
});
