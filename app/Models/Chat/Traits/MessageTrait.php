<?php

namespace App\Models\Chat\Traits;

use App\Models\Chat\Room;
use App\Models\Users\User;

/**
 * Trait MessageTrait
 *
 * @package App\Models\Chat\Traits
 */
trait MessageTrait
{

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return mixed
     */
    public function room()
    {
        return $this->belongsTo(Room::class, 'room_id', 'id');
    }

}