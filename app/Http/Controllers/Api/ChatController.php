<?php

namespace App\Http\Controllers\Api;

use App\Events\ChatMessageSent;
use App\Models\Chat\Room;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChatController extends Controller
{
    /**
     * @return string
     */
    public function myRooms()
    {
        $rooms = \Auth::user()->chatRooms;
        $rooms->load('participants:id,name');
        return $rooms;
    }

    /**
     * @param \App\Models\Chat\Room $room
     *
     * @return \App\Models\Chat\Room
     */
    public function room(Room $room)
    {
        return $room;
    }

    /**
     * @param \App\Models\Chat\Room $room
     *
     * @return \App\Models\Chat\Message[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public function messages(Room $room)
    {
        $room->load(['messages.user:id,name']);

        return $room->messages;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Chat\Room    $room
     *
     * @return \App\Models\Chat\Message
     */
    public function send(Request $request, Room $room)
    {
        /** @var \App\Models\Chat\Message $message */
        $message = $request->user()->chatMessages()->create([
            'room_id' => $room->id,
            'message' => $request->get('message'),
        ]);

        $room->nowActive();

        $message->load('user:id,name');

        ChatMessageSent::dispatch($message);

        return $message;
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function crossLine(Request $request)
    {
        $doWeHaveRoom = \DB::table('chat_room_user as my_rooms')
            ->join('chat_room_user as partners', 'partners.room_id', '=', 'my_rooms.room_id')
            ->where('my_rooms.user_id', \Auth::id())
            ->where('partners.user_id', $request->get('partner'))
            ->first();

        if ($doWeHaveRoom) {
            return ['redirect' => route('chat.room', $doWeHaveRoom->room_id)];
        }

        /** @var \App\Models\Chat\Room $newRoom */
        $newRoom = \Auth::user()->chatRooms()->create();
        $newRoom->nowActive();

        $newRoom->participants()->attach($request->get('partner'));

        return ['redirect' => route('chat.room', $newRoom->id)];
    }
}
