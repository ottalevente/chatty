@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">My Chat Rooms</div>

                    <div class="card-body">

                        <ul>
                            @foreach($myRooms as $room)

                                <li>

                                    <a href="{{ route('chat.inroom', $room->id) }}">

                                        @foreach(array_diff($room->participants->pluck('name')->toArray(), [\Auth::user()->name]) as $user)

                                            {{ $user }}@if (!$loop->last),@endif

                                        @endforeach

                                    </a>

                                </li>

                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
