@extends('layouts.app')

@section('content')
    @isset($room)
        <full-chat :data-room="{{ $room }}"></full-chat>
    @else
        <full-chat></full-chat>
    @endif
@endsection