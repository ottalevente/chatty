<?php

namespace App\Models\Chat;

use App\Models\Chat\Traits\RoomTrait;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use RoomTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'chat_rooms';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'last_activity',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'last_message',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'messages'
    ];

    /**
     * Update last_activity column to current datetime.
     */
    public function nowActive()
    {
        $this->last_activity = now();
        $this->save();
    }

    /**
     * @return mixed
     */
    public function getLastMessageAttribute()
    {
        return $this->messages->last();
    }
}
