<?php

namespace App\Http\Controllers;

use App\Models\Chat\Room;

class ChatController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('vue.chat');
    }

    /**
     * @param \App\Models\Chat\Room $room
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function room(Room $room)
    {
        return view('vue.chat', compact('room'));
    }
}
