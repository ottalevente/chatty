@extends('layouts.app')

@section('content')

    <chat :data-room="{{ $room }}"></chat>

@endsection
