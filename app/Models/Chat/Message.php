<?php

namespace App\Models\Chat;

use App\Models\Chat\Traits\MessageTrait;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use MessageTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'chat_messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message',
        'room_id',
    ];

}
