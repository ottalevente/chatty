<?php

namespace App\Models\Users\Traits;

use App\Models\Chat\Message;
use App\Models\Chat\Room;

/**
 * Trait ChatTrait
 *
 * @package App\Models\Users
 */
trait ChatTrait
{

    /**
     * @return mixed
     */
    public function chatRooms()
    {
        return $this->belongsToMany(Room::class, 'chat_room_user', 'user_id', 'room_id')
            ->orderByDesc('last_activity')
            ->withPivot('seen');
    }

    /**
     * @return mixed
     */
    public function chatMessages()
    {
        return $this->hasMany(Message::class, 'user_id', 'id');
    }

}