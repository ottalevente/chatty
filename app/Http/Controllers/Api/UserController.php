<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Users\User;

class UserController extends Controller
{
    /**
     * @return string
     */
    public function users()
    {
        return User::whereNotIn('id', [\Auth::id()])->get()->toJson();
    }
}
