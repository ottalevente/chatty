<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('last_activity')->nullable();
            $table->timestamps();
        });

        Schema::create('chat_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('room_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->text('message')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('room_id')
                ->references('id')
                ->on('chat_rooms');
        });

        Schema::create('chat_room_user', function (Blueprint $table) {
            $table->unsignedInteger('room_id');
            $table->unsignedInteger('user_id');
            $table->boolean('seen')->default(false);

            $table->primary(['room_id', 'user_id']);

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('room_id')
                ->references('id')
                ->on('chat_rooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_rooms');
        Schema::dropIfExists('chat_messages');
        Schema::dropIfExists('chat_room_user');
    }
}
