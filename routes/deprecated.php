<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::get('users', function() {
    $users = \App\Models\Users\User::whereNotIn('id', [\Auth::id()])->get();

    return view('users', compact('users'));

})->name('users')->middleware('auth');

Route::group(['prefix' => 'chat', 'middleware' => 'auth', 'as' => 'chat.'], function() {

    Route::post('start', function(\Illuminate\Http\Request $request) {

        $doWeHaveRoom = DB::table('chat_room_user as my_rooms')
            ->join('chat_room_user as partners', 'partners.room_id', '=', 'my_rooms.room_id')
            ->where('my_rooms.user_id', \Auth::id())
            ->where('partners.user_id', $request->get('partner'))
            ->first();

        if ($doWeHaveRoom) {
            return redirect(route('chat.inroom', $doWeHaveRoom->room_id));
        }

        /** @var \App\Models\Chat\Room $newRoom */
        $newRoom = \Auth::user()->chatRooms()->create();

        $newRoom->participants()->attach($request->get('partner'));

        return redirect(route('chat.inroom', $newRoom->id));

    })->name('start');

    Route::get('rooms', function() {

        $myRooms = \Auth::user()->chatRooms;

        return view('myrooms', compact('myRooms'));

    })->name('rooms');

    Route::get('rooms/{room}', function(\App\Models\Chat\Room $room) {

        return view('inroom', compact('room'));

    })->name('inroom');

});

// API

Route::group(['prefix' => 'api', 'middleware' => 'auth'], function() {

    Route::get('chat/rooms/{room}', function(\App\Models\Chat\Room $room) {

        $room->load(['messages', 'messages.user:id,name']);

        return $room;

    });

    Route::put('chat/rooms/{room}', function(\Illuminate\Http\Request $request, \App\Models\Chat\Room $room) {

        /** @var \App\Models\Chat\Message $message */
        $message = $request->user()->chatMessages()->create([
            'room_id' => $room->id,
            'message' => $request->get('message')
        ]);

        $message->load('user:id,name');

        \App\Events\ChatMessageSent::dispatch($message);

        return $message;

    });

});