<?php

Route::get('/', function() {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::get('users', function() {
    return view('vue.users');
})->name('users')->middleware('auth');

Route::get('chat', 'ChatController@index')
    ->name('chat')
    ->middleware('auth');
Route::get('chat/{room}', 'ChatController@room')
    ->name('chat.room')
    ->middleware('auth');

// API

Route::group(['prefix' => 'api', 'middleware' => 'auth'], function() {
    Route::get('users', 'Api\UserController@users');

    Route::group(['prefix' => 'chat'], function() {
        Route::get('/', 'Api\ChatController@myRooms');
        Route::post('/', 'Api\ChatController@crossLine');
        Route::get('{room}', 'Api\ChatController@room');
        Route::get('{room}/messages', 'Api\ChatController@messages');
        Route::put('{room}', 'Api\ChatController@send');
    });
});